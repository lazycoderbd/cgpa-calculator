package data;



import java.util.ArrayList;

import model.Semester;
import model.Subject;

public class ResultData {

	//store some temp data

	private String semesterName;
	private ArrayList<Subject> subjects;
	private ArrayList<Semester> semesters;
	private int semesterTotalCreadit;

	public ResultData(){

		semesters = new ArrayList<Semester>();

		//Subject(String subjectName, double subjectGPA, int courseCredit)
		{		
			subjects = new ArrayList<Subject>();
			Semester semester;
			semesterName = "Fall-2017";
			semesterTotalCreadit = 11;
			
			subjects.add(new Subject("Computer Fundamentals with lab",3.25,4));
			subjects.add(new Subject("Introduction to Software Engineering",3.00,3));
			subjects.add(new Subject("Physics with Lab",2.50,4));
			semesters.add(new Semester(subjects,semesterName,semesterTotalCreadit));
		}
		
		{		
			subjects = new ArrayList<Subject>();
			semesterName = "Spring 2015";
			semesterTotalCreadit = 13;
			
			subjects.add(new Subject("Mathematics-I (Calculus & Differential Equation) ",3.50,3));
			subjects.add(new Subject("Programming Language with Lab",3.75,4));
			subjects.add(new Subject("English Language",3.75,3));
			subjects.add(new Subject("Software Requirement Analysis and Design",3.75,3));
			
			semesters.add(new Semester(subjects,semesterName,semesterTotalCreadit));
		}
		
		{		
			subjects = new ArrayList<Subject>();
			semesterName = "Summer 2015";
			semesterTotalCreadit = 13;
			
			subjects.add(new Subject("Data Structures with Lab  ",4,4));
			subjects.add(new Subject("Statistics and Probabilities",3.50,3));
			subjects.add(new Subject("Mathematics II",3.00,3));
			subjects.add(new Subject("Software Engineering Project-l (using C)",4.00,3));
			
			semesters.add(new Semester(subjects,semesterName,semesterTotalCreadit));
		}
		
		{		
			subjects = new ArrayList<Subject>();
			semesterName = "Fall 2015 ";
			semesterTotalCreadit = 12;
			
			subjects.add(new Subject("Introduction to Database with Lab",3.75,4));
			subjects.add(new Subject("Java Programming with Lab",4.00,4));
			subjects.add(new Subject("Computer Algorithms with Lab",4.00,4));
			
			semesters.add(new Semester(subjects,semesterName,semesterTotalCreadit));
		}
		{		
			subjects = new ArrayList<Subject>();
			semesterName = "Spring 2016";
			semesterTotalCreadit = 14;
			
			subjects.add(new Subject("Software Engineering Quality Assurance and Testing",3.25,3));
			subjects.add(new Subject("Discrete Mathematics",3.75,3));
			subjects.add(new Subject("Object Oriented Concepts & Design with Lab",4.00,4));
			subjects.add(new Subject("Digital Electronics with Lab",2.50,4));
			
			semesters.add(new Semester(subjects,semesterName,semesterTotalCreadit));
		}
		{		
			subjects = new ArrayList<Subject>();
			semesterName = "Summer 2016";
			semesterTotalCreadit = 13;
			
			subjects.add(new Subject("Documentation of Software Engineering",2.75,3));
			subjects.add(new Subject("Principles of Accounting",4.00,3));
			subjects.add(new Subject("Software Project Management",3.00,3));
			subjects.add(new Subject("Operating Systems with Lab",4.00,4));
			
			semesters.add(new Semester(subjects,semesterName,semesterTotalCreadit));
		}
		
		{		
			subjects = new ArrayList<Subject>();
			semesterName = "Fall 2016";
			semesterTotalCreadit = 13;
			
			subjects.add(new Subject(".Net Programming with Lab",4.00,4));
			subjects.add(new Subject("Theory of Computing",4.00,3));
			subjects.add(new Subject("System Analysis & Design",3.50,3));
			subjects.add(new Subject("Software Security",3.00,3));
			
			semesters.add(new Semester(subjects,semesterName,semesterTotalCreadit));
		}		{		
			subjects = new ArrayList<Subject>();
			semesterName = "Spring 2017";
			semesterTotalCreadit = 14;
			
			subjects.add(new Subject("Data Communication With Lab",3.25,4));
			subjects.add(new Subject("Computer Architecture & Organization",2.75,3));
			subjects.add(new Subject("Desktop & Web Programming with Lab",3.75,4));
			subjects.add(new Subject("Software Engineering & Cyber Laws",3.25,3));
			
			semesters.add(new Semester(subjects,semesterName,semesterTotalCreadit));
		}		{		
			subjects = new ArrayList<Subject>();
			semesterName = "Summer 2017";
			semesterTotalCreadit = 14;
			
			subjects.add(new Subject("Artificial Intelligence with Lab",3.50,4));
			subjects.add(new Subject("Management Information System",3.00,3));
			subjects.add(new Subject("Object Oriented Software Development (Lab Based)",4.00,3));
			subjects.add(new Subject("Numerical Analysis with Lab",3.50,4));
			
			semesters.add(new Semester(subjects,semesterName,semesterTotalCreadit));
		}		
		
		{		
			subjects = new ArrayList<Subject>();
			semesterName = "Fall 2017";
			semesterTotalCreadit = 15;
			
			subjects.add(new Subject("Telecommunicaion Engineering with Lab",4.00,4));
			subjects.add(new Subject("Software Engineering Project-II (Web Programming)",4.00,3));
			subjects.add(new Subject("Distributive Computing and Networking Security with Lab",2.75,4));
			subjects.add(new Subject("Advanced Database Management Systems with Lab",3.50,4));
			
			semesters.add(new Semester(subjects,semesterName,semesterTotalCreadit));
		}	
		
		{		
			subjects = new ArrayList<Subject>();
			semesterName = "Spring 2018";
			semesterTotalCreadit = 4;
			
			subjects.add(new Subject("Networking",4.00,4));
			
			semesters.add(new Semester(subjects,semesterName,semesterTotalCreadit));
		}
		
		{		
			subjects = new ArrayList<Subject>();
			semesterName = "Summer 2018";
			semesterTotalCreadit = 3;
			
			subjects.add(new Subject("Final project",4.0,3));
			
			semesters.add(new Semester(subjects,semesterName,semesterTotalCreadit));
		}
		
		

	}

	public void show(){
		
		
		sop("\n\n\t\t\t\t\tProgram:         B.Sc. in Software Engineering");
		sop("\t\t\t\t\tName of Student: Syed Ashraf Ullah");
		sop("\t\t\t\t\tStudent Id:      143-35-789");
		sop("\t\t\t\t\tEnrollment:      Fall 2014");
		sop("\t\t\t\t\tBatch:           15");
		
		sop("\n\n");
		
		int totalCreadit = 0;
		
		double totalGPA = 0;
		int semesterNo = 0;
		
		for(Semester semester: semesters){
			sop("Course Title \t	Credit \t Grade Point\n");
			semesterNo++;
			
			int semesterTotalSubject = 0;
			double semesterTotalGradePoint = 0;
			double SGPA = 0;
			for(Subject subject: semester.getSubjects()){
				sop(subject.getSubjectName()+" \t "+subject.getCourseCredit()+"\t"+subject.getSubjectGPA());
				semesterTotalGradePoint += subject.getSubjectGPA()*subject.getCourseCredit();
								
				semesterTotalSubject++;
			}
			
			SGPA = semesterTotalGradePoint/semester.getTotalCreadit();
			totalCreadit += semester.getTotalCreadit();
			totalGPA += semesterTotalGradePoint;
			
			sop("\nTotal Credits Taken : "+totalCreadit+"\tSemester NO: "+semesterNo+" \t SGPA : "+SGPA+"\t CGPA "+totalGPA/totalCreadit+"\n\n");
			
		}
	}
	
	public static void sop(String message){
		System.out.println(message);
	}
	

}
