package model;

import java.util.ArrayList;
import java.util.Arrays;

public class Semester {
	private ArrayList<Subject> subjects;
	private String semesterName;
	private int totalCreadit;
	
	
	
	
	public Semester(ArrayList<Subject> subjects, String semesterName,
			int totalCreadit) {
		super();
		this.subjects = subjects;
		this.semesterName = semesterName;
		this.totalCreadit = totalCreadit;
	}
	
	

	public int getTotalCreadit() {
		return totalCreadit;
	}



	public void setTotalCreadit(int totalCreadit) {
		this.totalCreadit = totalCreadit;
	}



	public ArrayList<Subject> getSubjects() {
		return subjects;
	}

	public void setSubjects(ArrayList<Subject> subjects) {
		this.subjects = subjects;
	}
	public String getSemesterName() {
		return semesterName;
	}

	public void setSemesterName(String semesterName) {
		this.semesterName = semesterName;
	}

	public String toString() {
		return "Semester [subjects="
				+ (subjects != null ? Arrays.asList(subjects) : null)
				+ ", semesterName=" + semesterName + "]";
	}
	
	
}
