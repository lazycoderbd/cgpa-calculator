package model;

/**
 * @author lazycoder
 *
 */
public class Subject {
	
	private String subjectName;
	private double subjectGPA;
	private int courseCredit;
	
	public Subject(){
		
	}
	
	public Subject(String subjectName, double subjectGPA, int courseCredit) {
		super();
		this.subjectName = subjectName;
		this.subjectGPA = subjectGPA;
		this.courseCredit = courseCredit;
	}
	public String getSubjectName() {
		return subjectName;
	}
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	public double getSubjectGPA() {
		return subjectGPA;
	}
	public void setSubjectGPA(double subjectGPA) {
		this.subjectGPA = subjectGPA;
	}
	public int getCourseCredit() {
		return courseCredit;
	}
	public void setCourseCredit(int courseCredit) {
		this.courseCredit = courseCredit;
	}

	@Override
	public String toString() {
		return "Subject [subjectName=" + subjectName + ", subjectGPA="
				+ subjectGPA + ", courseCredit=" + courseCredit + "]";
	}
	
	
	
	
	
	
	
}
