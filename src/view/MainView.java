package view;

public interface MainView {
	void showCGPA();
	void showTotalGrade();
	void showTotalPoint();
	void showSGPAWithSubjectName();
}
